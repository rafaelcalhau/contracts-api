import Sequelize, { Model } from "sequelize";
import { sequelize } from "@config/db";
import Contract from "./contract.entity";

class Job extends Model {
  ContractId: number;
  description: string;
  paid: boolean;
  paymentDate: Date;
  price: number;

  contract?: Contract
}
Job.init(
  {
    description: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    price:{
      type: Sequelize.DECIMAL(12,2),
      allowNull: false
    },
    paid: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    paymentDate:{
      type: Sequelize.DATE
    }
  },
  {
    sequelize,
    modelName: 'Job'
  }
);

export default Job;
