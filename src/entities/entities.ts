import Contract from "./contract.entity";
import Job from "./job.entity";
import Profile from "./profile.entity";

Profile.hasMany(Contract, { as: 'contractor', foreignKey: 'ContractorId' });
Contract.belongsTo(Profile, { as: 'contractor' });
Profile.hasMany(Contract, { as: 'client', foreignKey: 'ClientId' });
Contract.belongsTo(Profile, { as: 'client' });
Contract.hasMany(Job, { as: 'contract', foreignKey: 'ContractId' });
Job.belongsTo(Contract, { as: 'contract' });

export default {
  Contract,
  Job,
  Profile,
};
