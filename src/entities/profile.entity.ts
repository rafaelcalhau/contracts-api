import Sequelize, { Model } from 'sequelize';
import { sequelize } from '@config/db';

export const PROFILE_TYPES = {
  CLIENT: 'client',
  CONTRACTOR: 'contractor',
}

class Profile extends Model {
  id: number;
  balance: number;
  firstName: string;
  lastName: string;
  profession: string;
  type: 'client' | 'contractor';
}
Profile.init(
  {
    firstName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    lastName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    profession: {
      type: Sequelize.STRING,
      allowNull: false
    },
    balance:{
      type:Sequelize.DECIMAL(12,2)
    },
    type: {
      type: Sequelize.ENUM('client', 'contractor')
    }
  },
  {
    sequelize,
    modelName: 'Profile'
  }
);

export default Profile;
