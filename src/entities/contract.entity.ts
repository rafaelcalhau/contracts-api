import Sequelize, { Model } from "sequelize";
import { sequelize } from "@config/db";
import Profile from "./profile.entity";

export interface ContractInstance extends Contract { id: number }

export const CONTRACT_STATUSES = {
  NEW: 'new',
  IN_PROGRESS: 'in_progress',
  TERMINATED: 'terminated',
};

class Contract extends Model {
  id: number
  ClientID: number
  ContractorID: number

  client?: Profile
  contractor?: Profile
}
Contract.init(
  {
    terms: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    status:{
      type: Sequelize.ENUM('new','in_progress','terminated')
    }
  },
  {
    sequelize,
    modelName: 'Contract'
  }
);

export default Contract;
