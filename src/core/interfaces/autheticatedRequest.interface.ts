import { Request } from "express";
import Profile from "@entities/profile.entity";

type ProfileInstance = Profile & { id: number };
export interface AuthenticatedRequest extends Request {
  profile?: ProfileInstance
}