import { Express } from "express";
import ContractsRouter from "@modules/contracts/contracts.router";
import JobsRouter from "@modules/jobs/jobs.router";
import BalancesRouter from "@modules/balances/balances.router";
import AdminsRouter from "@modules/admins/admins.router";

export default function (app: Express) {
  const routers = [
    AdminsRouter,
    BalancesRouter,
    ContractsRouter,
    JobsRouter
  ];

  routers.forEach(router => router(app));
}
