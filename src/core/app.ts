import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import { sequelize } from '@config/db';
import models from '@entities/entities'; 
import buildRoutes from './routes';
const app = express();
app.use(helmet());

app.use(bodyParser.json());
app.set('sequelize', sequelize);
app.set('models', models);

buildRoutes(app);

export default app;
