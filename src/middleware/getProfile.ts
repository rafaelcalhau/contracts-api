import { NextFunction, Response } from "express";
import { AuthenticatedRequest } from "@core/interfaces/autheticatedRequest.interface";

const getProfile = async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
    const { Profile } = req.app.get('models');
    const profile = await Profile.findOne({ where: { id: req.get('profile_id') || 0 } });

    if (!profile) {
        return res.status(401).end();
    }

    req.profile = profile;
    next();
};

export default getProfile;
