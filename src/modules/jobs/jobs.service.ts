import { Op, Sequelize } from "sequelize";
import Contract, { ContractInstance } from "@entities/contract.entity";
import Job from "@entities/job.entity";
import Profile, { PROFILE_TYPES } from "@entities/profile.entity";

export class JobsService {
  constructor (
    private readonly contract: typeof Contract,
    private readonly job: typeof Job,
    private readonly profile: typeof Profile,
    private readonly db: Sequelize,
  ) {}
  
  /**
   * Get all unpaid jobs.
   * @param profileId: number
   * @returns Job[]
   */
  async getUnpaidJobs (profileId: number): Promise<Job[]> {
    const userContractList = await this.contract.findAll({
      attributes: ['id'],
      where: {
        [Op.or]: [
          { ContractorID: profileId },
          { ClientId: profileId }
        ]
      },
      raw: true
    });

    return await this.job.findAll({
      where: {
        paid: false,
        ContractID: {
          [Op.in]: userContractList.map((item: ContractInstance) => item.id)
        }
      }
    });
  }

  /**
   * Make a payment for a job
   * @param jobId: number
   * @returns Job
   */
   async registerPayment (jobId: number): Promise<Job> {
    // Rules:
    // - Pay for a job, a client can only pay if his balance >= the amount to pay.
    // - The amount should be moved from the client's balance to the contractor balance.

    const client = await this.profile.findOne({
      attributes: ['id', 'balance', 'type'],
      where: {
        type: PROFILE_TYPES.CLIENT
      },
    });

    if (!client) {
      throw new Error('User is not a client type.');
    }

    const job = await this.job.findOne({
      where: { id: jobId, paid: false },
      include: [{
        as: 'contract',
        model: Contract,
        foreignKey: 'ContractID',
        attributes: ['id', 'ContractorID'],
      }],
    });
    if (!job) {
      throw new Error('Job does not exists or is already paid.');
    }

    if (client.balance < job.price) {
      throw new Error('Client does not have enough funds to pay this job.');
    }

    const contractor = await this.profile.findByPk(job.contract.get('ContractorID'));
    const transaction = await this.db.transaction();

    try {
      await job.update({ paid: true }, { transaction });
      await client.update({ balance: client.balance - job.price }, { transaction });
      await contractor.update({ balance: contractor.balance + job.price }, { transaction });
      await transaction.commit();
      await job.reload();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }

    return job;
  }
}
