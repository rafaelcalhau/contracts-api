import { Express, Router } from "express";
import { sequelize } from "@config/db";
import { JobsController } from "@modules/jobs/jobs.controller";
import getProfile from "@middleware/getProfile";
import { JobsService } from "./jobs.service";

export default function (app: Express) {
  const { Contract, Job, Profile } = app.get('models');
  const service = new JobsService(Contract, Job, Profile, sequelize)
  const controller = new JobsController(service);
  const router = Router();
  app.use(router);

  /**
   * Get all unpaid jobs.
   * @returns Jobs
   */
  router.get('/jobs', getProfile, controller.getUnpaidJobs);

  /**
   * Make a payment for a job
   * @returns Job
   */
   router.post('/jobs/:job_id/pay', getProfile, controller.payJob);
}
