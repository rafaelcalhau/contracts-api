import { Response } from 'express';
import { AuthenticatedRequest } from '@core/interfaces/autheticatedRequest.interface';
import { JobsService } from './jobs.service';

export class JobsController {
  constructor (
    private service: JobsService
  ) {
    this.getUnpaidJobs = this.getUnpaidJobs.bind(this);
    this.payJob = this.payJob.bind(this);
  }

  /**
   * Get profile's non terminated contracts.
   * @returns Job[]
   */
  async getUnpaidJobs (req: AuthenticatedRequest, res: Response): Promise<unknown> {
    const profileId = req.profile.id;

    try {
      const jobs = await this.service.getUnpaidJobs(profileId);
      return res.json(jobs);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  }

  /**
   * Make a payment for a job
   * @returns Job
   */
   async payJob (req: AuthenticatedRequest, res: Response): Promise<unknown> {
    const jobId = Number(req.params.job_id);

    try {
      const job = await this.service.registerPayment(jobId);
      return res.json(job);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  }
}