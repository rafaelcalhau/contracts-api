import { Op } from "sequelize";
import Contract, { ContractInstance } from "@entities/contract.entity";
import Job from "@entities/job.entity";
import Profile, { PROFILE_TYPES } from "@entities/profile.entity";
import { BalanceDepositResponse } from "./dto/balanceDepositResponse.dto";

export class BalancesService {
  constructor (
    private readonly contract: typeof Contract,
    private readonly job: typeof Job,
    private readonly profile: typeof Profile
  ) {}

  /**
   * Deposits money into the the the balance of a client.
   * @param clientId: number
   * @param amount: number
   * @returns BalanceDepositResponse
   */
   async deposit (clientId: number, amount: number): Promise<BalanceDepositResponse> {
    // Rules:
    // - Deposits money into the the the balance of a client
    // - a client can't deposit more than 25% his total of jobs to pay. (at the deposit moment)

    const client = await this.profile.findOne({
      attributes: ['id', 'balance', 'type'],
      where: {
        id: clientId,
        type: PROFILE_TYPES.CLIENT
      },
    });

    if (!client) {
      throw new Error('Client does not exists.');
    }

    const contracts = await this.contract.findAll({
      attributes: ['id'],
      where: {
        ClientID: clientId
      },
      raw: true,
    });

    const nonPaidJobsAmount = await this.job.sum('price', {
      where: { contractID: { [Op.in]: contracts.map((item: ContractInstance) => item.id) } }
    });

    const depositLimit = Number((nonPaidJobsAmount * 0.25).toFixed(2));
    if (depositLimit < amount) {
      throw new Error(`The amount is higher than the allowed limit: $${depositLimit}.`);
    }

    await client.update({ balance: client.balance + amount });
    await client.reload();

    return {
      clientId,
      amount
    }
  }
}
