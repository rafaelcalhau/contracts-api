export interface BalanceDepositResponse {
  clientId: number;
  amount: number
}