import { Express, Router } from "express";
import { BalancesController } from "@modules/balances/balances.controller";
import getProfile from "@middleware/getProfile";
import { BalancesService } from "./balances.service";

export default function (app: Express) {
  const { Contract, Job, Profile } = app.get('models');
  const service = new BalancesService(Contract, Job, Profile)
  const controller = new BalancesController(service);
  const router = Router();
  app.use(router);

  /**
   * Deposits money into the the the balance of a client.
   * @returns BalanceDepositResponse
   */
   router.post('/balances/deposit/:userId', getProfile, controller.deposit);
}
