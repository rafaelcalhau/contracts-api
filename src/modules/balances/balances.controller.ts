import { Response } from 'express';
import { AuthenticatedRequest } from '@core/interfaces/autheticatedRequest.interface';
import { BalancesService } from './balances.service';

export class BalancesController {
  constructor (
    private service: BalancesService
  ) {
    this.deposit = this.deposit.bind(this);
  }

  /**
   * Deposits money into the the the balance of a client.
   * @returns BalanceDepositResponse
   */
   async deposit (req: AuthenticatedRequest, res: Response): Promise<unknown> {
    const clientId = Number(req.params.userId);
    const amount = Number(req.body.amount);

    try {
      const deposit = await this.service.deposit(clientId, amount);
      return res.json(deposit);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  }
}