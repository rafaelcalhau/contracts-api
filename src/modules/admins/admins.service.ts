import Profile, { PROFILE_TYPES } from "@entities/profile.entity";
import { BestClientResponse } from "./dto/bestClientResponse.dto";
import { BestProfessionResponse } from "./dto/bestProfessionResponse.dto";

export class AdminsService {
  constructor (
    private readonly profile: typeof Profile
  ) {}

  /**
   * Returns the clients that paid the most for jobs in the query time period.
   * @param start: string
   * @param end: string
   * @param limit: number
   * @returns BestClientResponse[]
   */
  async getBestClients (start: string, end: string, limit: number): Promise<BestClientResponse[]> {
    // Rules:
    // - Returns the clients the paid the most for jobs in the query time period.
    // - Limit query parameter should be applied, default limit is 2.

    // validate the date format
    let fromDate: string
    let toDate: string;

    try {
      fromDate = new Date(start).toISOString();
      toDate = new Date(end).toISOString();
    } catch (error) {
      throw new Error('Invalid date range values informed.');
    }

    const [clientsMostPaid] = await this.profile.sequelize.query(/* SQL */`
      SELECT
        p.id,
        p.firstName,
        p.lastName,
        sum(j.price) as totalPaid
      FROM Profiles p
      JOIN Contracts c on c.ClientID = p.id
      JOIN Jobs j on j.ContractID = c.id
      WHERE
        p.type = '${PROFILE_TYPES.CLIENT}'
        AND j.paid = true
        AND j.paymentDate BETWEEN '${fromDate}' AND '${toDate}'
      GROUP BY
        p.id
      ORDER BY
        totalPaid DESC
      LIMIT ${limit}
    `);

    return clientsMostPaid.map((client: any) => ({
      id: client.id,
      fullName: `${client.firstName} ${client.lastName}`,
      paid: client.totalPaid
    }));
  }

  /**
   * Returns the profession that earned the most money.
   * @param start: string
   * @param end: string
   * @returns BestProfessionResponse|null
   */
  async getBestProfession (start: string, end: string): Promise<BestProfessionResponse|null> {
    // Rules:
    // - Returns the profession that earned the most money (sum of jobs paid)
    // for any contractor that worked in the query time range.

    // validate the date format
    let fromDate: string
    let toDate: string;

    try {
      fromDate = new Date(start).toISOString();
      toDate = new Date(end).toISOString();
    } catch (error) {
      throw new Error('Invalid date range values informed.');
    }

    const [bestProfession] = await this.profile.sequelize.query(/* SQL */`
      SELECT
        p.id,
        p.profession,
        sum(j.price) as totalEarned
      FROM Profiles p
      JOIN Contracts c on c.ContractorID = p.id
      JOIN Jobs j on j.ContractID = c.id
      WHERE
        p.type = '${PROFILE_TYPES.CONTRACTOR}'
        AND j.paid = true
        AND j.paymentDate BETWEEN '${fromDate}' AND '${toDate}'
      GROUP BY
        p.id
      ORDER BY
      totalEarned DESC
      LIMIT 1
    `);

    if (bestProfession.length !== 1) return null;
    const { profession } = bestProfession[0] as any;
    return { profession }
  }
}
