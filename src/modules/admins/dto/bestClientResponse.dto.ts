export interface BestClientResponse {
  id: number
  fullName: string
  paid: number
}