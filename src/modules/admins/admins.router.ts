import { Express, Router } from "express";
import { AdminsController } from "@modules/admins/admins.controller";
import getProfile from "@middleware/getProfile";
import { AdminsService } from "./admins.service";

export default function (app: Express) {
  const { Profile } = app.get('models');
  const service = new AdminsService(Profile)
  const controller = new AdminsController(service);
  const router = Router();
  app.use(router);

  /**
   * Returns the clients that paid the most for jobs in the query time period.
   * @returns BestClientResponse[]
   */
  router.get('/admin/best-clients', getProfile, controller.getBestClients);

  /**
   * Returns the profession that earned the most money in the query time period.
   * @returns BestProfessionResponse
   */
  router.get('/admin/best-profession', getProfile, controller.getBestProfession);
}
