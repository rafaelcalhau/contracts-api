import { Response } from 'express';
import { AuthenticatedRequest } from '@core/interfaces/autheticatedRequest.interface';
import { AdminsService } from './admins.service';

export class AdminsController {
  constructor (
    private service: AdminsService
  ) {
    this.getBestClients = this.getBestClients.bind(this);
    this.getBestProfession = this.getBestProfession.bind(this);
  }

  /**
   * Returns the clients that paid the most for jobs in the query time period.
   * @returns BestClientResponse[]
   */
  async getBestClients (req: AuthenticatedRequest, res: Response): Promise<unknown> {
    const fromDate = req.query.start ? String(req.query.start) : '';
    const toDate = req.query.end ? String(req.query.end) : '';
    const limit = req.query.limit ? Number(req.query.limit) : 2;

    try {
      const clients = await this.service.getBestClients(fromDate, toDate, limit);
      return res.json(clients);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  }

  /**
   * Returns the profession that earned the most money.
   * @returns BestProfessionResponse
   */
  async getBestProfession (req: AuthenticatedRequest, res: Response): Promise<unknown> {
    const fromDate = req.query.start ? String(req.query.start) : '';
    const toDate = req.query.end ? String(req.query.end) : '';

    try {
      const profession = await this.service.getBestProfession(fromDate, toDate);
      if (!profession) {
        return res.status(404).json({ message: 'No profession was found in the given date range.' });
      }
      return res.json(profession);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  }
}