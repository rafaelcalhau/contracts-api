import { Express, Router } from "express";
import { ContractsController } from "@modules/contracts/contracts.controller";
import getProfile from "@middleware/getProfile";
import { ContractsService } from "./contracts.service";

export default function (app: Express) {
  const { Contract: contract } = app.get('models');
  const service = new ContractsService(contract)
  const controller = new ContractsController(service);
  const router = Router();
  app.use(router);

  /**
   * Get profile's non terminated contracts.
   * @returns contract by id
   */
  router.get('/contracts', getProfile, controller.getContracts);

  /**
   * Get a single contract.
   * @returns contract by id
   */
  router.get('/contracts/:id', getProfile, controller.getContractById);
}
