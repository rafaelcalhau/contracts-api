import { Response } from 'express';
import { AuthenticatedRequest } from '@core/interfaces/autheticatedRequest.interface';
import { ContractsService } from './contracts.service';

export class ContractsController {
  constructor (
    private service: ContractsService
  ) {
    this.getContracts = this.getContracts.bind(this);
    this.getContractById = this.getContractById.bind(this);
  }

  /**
   * Get profile's non terminated contracts.
   * @returns Contract[]
   */
  async getContracts (req: AuthenticatedRequest, res: Response): Promise<unknown> {
    const profileId = req.profile.id;
    try {
      const contracts = await this.service.getContracts(profileId);
      return res.json(contracts);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  }

  /**
   * Get a single contract.
   * @returns Contract | NotFoundError
   */
  async getContractById (req: AuthenticatedRequest, res: Response): Promise<unknown> {
    const contractId = Number(req.params.id);
    const profileId = req.profile.id;
    try {
      const contract = await this.service.getContractById(contractId, profileId);

      if (!contract) return res.status(404).end();
      return res.json(contract);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  }
}