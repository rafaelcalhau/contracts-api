import { ContractsService } from "./contracts.service";

describe('ContractsService', () => {
  let service;
  const contractsCollection = [{
    "id": 1,
    "terms": "bla bla bla",
    "status": "terminated",
    "createdAt": "2022-10-22T01:10:14.009Z",
    "updatedAt": "2022-10-22T01:10:14.009Z",
    "ContractorId": 5,
    "ClientId": 1
  }];

  const contractModel = {
    findAll: jest.fn(),
    findOne: jest.fn(),
  };

  beforeAll(() => {
    service = new ContractsService(contractModel)
  })

  it('getContracts should call findAll from contract model', async () => {
    const profileId = 1;
    await service.getContracts(profileId);
    expect(contractModel.findAll).toHaveBeenCalledTimes(1);
  })

  it('getContractById should call findAll from contract model', async () => {
    const id = 1;
    const profileId = 1;
    await service.getContractById(id, profileId);
    expect(contractModel.findOne).toHaveBeenCalledTimes(1);
  })
});
