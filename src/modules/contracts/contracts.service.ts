import { Op } from "sequelize";
import Contract, { CONTRACT_STATUSES } from "@entities/contract.entity";

export class ContractsService {
  constructor (
    private readonly contract: typeof Contract
  ) {}
  
  /**
   * Get profile's non terminated contracts.
   * @param profileId: number
   * @returns Contract[]
   */
  async getContracts (profileId: number): Promise<Contract[]> {
    return await this.contract.findAll({
      where: {
        status: {
          [Op.ne]: CONTRACT_STATUSES.TERMINATED
        },
        [Op.or]: [
          { ContractorID: profileId },
          { ClientId: profileId }
        ]
      }
    });
  }

  /**
   * Get a single contract.
   * @param id: Number
   * @param profileId: Number
   * @returns Contract | null
   */
  async getContractById (id: number, profileId: number): Promise<Contract | null> {
    const contract = await this.contract.findOne({
      where: {
        id,
        [Op.or]: [
          { ContractorID: profileId },
          { ClientId: profileId }
        ]
      }
    });

    return contract;
  }
}
